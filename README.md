# README #

This is a project demo for a payments platform for the standard software engineer coding challenge. In this file you
will find the instructions to run the application and consume the REST API.

The demo was designed to be very easily deployable in a local environment so that it's very easy to try out. Only Java and Maven are required for its execution. 

It uses an H2 in-memory database. Which means that the data is not
persisted after the project is not running anymore.

## Dependencies

- Java 8 or higher
- Maven 3.6 or higher

## Execution Instructions

1. Clone this repository
2. To install the project dependencies, execute the command `mvn install` in the project root directory

3. To run the application, you can either:
   - run the `PaymentPlatformApplication` class in an IDE
   - build the application with `mvn package` and then run the `.jar` file located in `/target` e.g. `java -jar target/paymentplatform-0.0.1-SNAPSHOT.jar`

The application will run and expose its API in http://localhost:8080 and the antifraud and bank mock APIs will be deployed in http://localhost:8081 and  http://localhost:8082 respectively

Additionally, if you want to query the in-memory database, you can open http://localhost:8080/h2-console/ in your browser and login with the credentials found in the `application.properties` file
## REST API

There are three endpoints in the API, the first one submits a credit card payment, the second one gets the information
of an order, and the third one refunds an order. The external bank and antifraud services are mocked, so depending on
certain values sent in the body of the requests the purchases will have different results.

### 1. Submit credit card payment

Submits a credit card payment. To condition a specific behaviour from the antifraud and bank mocks, use these values in the request:

1. Antifraud service declination: CVV2 ***111***
2. Antifraud service approval and bank declination: CVV2 **222**
3. Transaction approval (if all parameters are valid): CVV2 **555**


_Note: the only existing account in the database has an id "1". To be approved, payments must use this `accountId` in the request as shown in the example._

- **REST method:** POST
- **PATH:** /payments
- **Headers:** Content-type: application/json
- **Request body example:**

```
{
    "accountId":"1",
    "purchaseValue": 100000,
    "taxValue": 1000,
    "currency": "COP",
    "creditCard": {
        "number": "4037997623271984",
        "cvv2": 555,
        "expirationDate": "2023/02",
        "cardHolderName": "John Doe",
        "franchise": "VISA"
    },
    "payer": {
        "documentNumber":"123123123123",
        "documentType":"CC",
        "name":"John Doe",
        "address": "Cra 1 # 2 - 3",
        "country":"CO",
        "email":"john.doe@email.com",
        "phoneNumber":"+57321321312"
    }
}
```
- **Response body example (Status 400)**:
```
{
    "orderId": "145161f8-d455-4481-9e03-f798441739fa",
    "transactionId": "6c3d4180-d955-4393-a617-41f5ba8ef653",
    "approvalStatus": "DECLINED",
    "errorMessage": "Antifraud validation failed for transaction"
}
```

### 2. Query payment result

Queries the result of a payment. This endpoint returns an order with all its associated transactions and the payer's information.

- **REST method:** GET
- **PATH:** /payments/{orderId}
- **Headers:** Content-type: application/json
- **Response body example (Status 200):**

```
{
    "order": {
        "orderId": "d88bf2e9-8fa9-47bc-94ed-a68daf40f471",
        "creationDate": "2022-02-07T00:16:06.745862",
        "status": "COMPLETED",
        "payer": {
            "payerId": "43e7cd98-e703-4f49-8a58-c50d5a229a8b",
            "documentNumber": "123123123123",
            "documentType": "CC",
            "creationDate": "2022-02-07T00:16:06.745781",
            "name": "John Doe",
            "address": "Cra 1 # 2 - 3",
            "country": "CO",
            "email": "john.doe@email.com",
            "phoneNumber": "+57321321312"
        },
        "transactions": [
            {
                "transactionId": "3dba479f-bc37-48c7-8933-2c221601d63f",
                "orderId": "d88bf2e9-8fa9-47bc-94ed-a68daf40f471",
                "creationDate": "2022-02-07T00:16:06.74585",
                "type": "AUTHORIZATION_AND_CAPTURE",
                "processingStatus": "COMPLETED",
                "approvalStatus": "APPROVED",
                "antifraudStatus": "ANTIFRAUD_ACCEPTED",
                "bankStatus": "BANK_ACCEPTED",
                "value": 100000.0,
                "taxValue": 1000.0,
                "currency": "COP",
                "networkValue": 101000.0,
                "creditCard": {
                    "creditCardId": "c4fb91ee-4f55-49db-8d33-461314ef2b31",
                    "creationDate": "2022-02-07T00:16:06.745808",
                    "expirationDate": "2023/02",
                    "number": "4037********1984",
                    "cvv2": "***",
                    "cardHolderName": "John Doe",
                    "franchise": "VISA"
                },
                "creditCardToken": "3818f2a8-e146-400f-b331-fca2d75512e2",
                "errorMessage": null
            },
            {
                "transactionId": "d0326f24-b3cf-4488-b705-523d57b66308",
                "orderId": "d88bf2e9-8fa9-47bc-94ed-a68daf40f471",
                "creationDate": "2022-02-07T00:16:36.492203",
                "type": "REFUND",
                "processingStatus": "COMPLETED",
                "approvalStatus": "APPROVED",
                "antifraudStatus": null,
                "bankStatus": "BANK_ACCEPTED",
                "value": 50000.0,
                "taxValue": null,
                "currency": "COP",
                "networkValue": 50000.0,
                "creditCard": null,
                "creditCardToken": null,
                "errorMessage": null
            }
        ]
    },
    "errorMessage": ""
}
```

### 1. Refund payment

Refunds a credit card payment. To condition a response from the bank mock, use the following values in the request:

1. Bank approval: any `refundValue` less than or equal to the original transaction value
2. Bank declination: a `refundValue` of 66666.66 (the payment value must be greater than or equal to that number)

- **REST method:** PUT
- **PATH:** /payments
- **Headers:** Content-type: application/json
- **Request body example:**
```
{
    "orderId":"d88bf2e9-8fa9-47bc-94ed-a68daf40f471",
    "refundValue":50000,
    "currency":"COP"
}
```
- **Response body example (Status 201)**
```
{
    "transactionDto": {
        "transactionId": "d0326f24-b3cf-4488-b705-523d57b66308",
        "orderId": "d88bf2e9-8fa9-47bc-94ed-a68daf40f471",
        "creationDate": "2022-02-07T00:16:36.492203",
        "type": "REFUND",
        "processingStatus": "COMPLETED",
        "approvalStatus": "APPROVED",
        "antifraudStatus": null,
        "bankStatus": "BANK_ACCEPTED",
        "value": 50000.0,
        "taxValue": null,
        "currency": "COP",
        "networkValue": 50000.0,
        "creditCard": null,
        "creditCardToken": null,
        "errorMessage": null
    },
    "errorMessage": ""
}
```


### Class diagram of the model
![Clas Diagram](/doc/class_diagram.png)