package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.*;
import com.diegoramos.paymentplatform.model.entity.BankStatus;
import com.diegoramos.paymentplatform.model.entity.TransactionEntity;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;

import java.time.LocalDateTime;

public class TransactionSample {

    public static TransactionEntity getCompletedApprovedTransactionEntity() {

        return TransactionEntity.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creditCardToken("eefe9a75-7371-48c9-b270-932139a06fdb")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardEntity())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.APPROVED)
                .build();
    }

    public static TransactionEntity getBankAcceptedTransactionEntity() {
        return TransactionEntity.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creditCardToken("eefe9a75-7371-48c9-b270-932139a06fdb")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardEntity())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionEntity getAntifraudAcceptedTransactionEntity() {
        return TransactionEntity.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardEntity())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionEntity getAntifraudDeclinedTransactionEntity() {
        return TransactionEntity.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardEntity())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_DECLINED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionEntity getBankDeclinedTransactionEntity() {
        return TransactionEntity.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardEntity())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .bankStatus(BankStatus.BANK_DECLINED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto.TransactionDtoBuilder getTransactionDtoBaseBuilder() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE);
    }

    public static TransactionDto getCompletedApprovedTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creditCardToken("eefe9a75-7371-48c9-b270-932139a06fdb")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.APPROVED)
                .build();
    }

    public static TransactionDto getCompletedAntifraudDeclinedTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_DECLINED)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.DECLINED)
                .errorMessage("Antifraud validation failed for transaction")
                .build();
    }

    public static TransactionDto getCompletedBankDeclinedTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .bankStatus(BankStatus.BANK_DECLINED)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.DECLINED)
                .errorMessage("Transaction declined by the bank")
                .build();
    }

    public static TransactionDto getBankAcceptedTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creditCardToken("eefe9a75-7371-48c9-b270-932139a06fdb")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto getAntifraudAcceptedTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto getAntifraudDeclinedTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_DECLINED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto getBankDeclinedTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .taxValue(2000.0)
                .creditCard(CreditCardSample.getSecureCreditCardDto())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .networkValue(12000.0)
                .antifraudStatus(AntifraudStatus.ANTIFRAUD_ACCEPTED)
                .bankStatus(BankStatus.BANK_DECLINED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto getInitialRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .type(TransactionType.REFUND)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto getCompletedAcceptedRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("387d2f00-df3b-4ca2-a03b-343eef374b8d")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .type(TransactionType.REFUND)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.APPROVED)
                .build();
    }

    public static TransactionDto getCompletedAcceptedPartialRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("387d2f00-df3b-4ca2-a03b-343eef374b8d")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(5000.0)
                .type(TransactionType.REFUND)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.APPROVED)
                .build();
    }

    public static TransactionDto getCompletedDeclinedRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("387d2f00-df3b-4ca2-a03b-343eef374b8d")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .type(TransactionType.REFUND)
                .bankStatus(BankStatus.BANK_DECLINED)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.DECLINED)
                .errorMessage("Transaction declined by the bank")
                .build();
    }

    public static TransactionDto getCompletedDeclinedInvalidValueRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("387d2f00-df3b-4ca2-a03b-343eef374b8d")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(13000.0)
                .type(TransactionType.REFUND)
                .processingStatus(ProcessingStatus.COMPLETED)
                .approvalStatus(ApprovalStatus.DECLINED)
                .errorMessage("The requested refund value of 13000.00 COP is greater than the original purchase value of 12000.00 COP")
                .build();
    }

    public static TransactionDto getDeclinedRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("387d2f00-df3b-4ca2-a03b-343eef374b8d")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .type(TransactionType.REFUND)
                .bankStatus(BankStatus.BANK_DECLINED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto getAcceptedRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("387d2f00-df3b-4ca2-a03b-343eef374b8d")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(10000.0)
                .type(TransactionType.REFUND)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

    public static TransactionDto getAcceptedPartialRefundTransactionDto() {
        return TransactionDto.builder()
                .transactionId("387d2f00-df3b-4ca2-a03b-343eef374b8d")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .currency(Currency.COP)
                .value(5000.0)
                .type(TransactionType.REFUND)
                .bankStatus(BankStatus.BANK_ACCEPTED)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .build();
    }

}
