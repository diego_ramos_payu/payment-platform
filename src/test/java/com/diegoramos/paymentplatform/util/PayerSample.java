package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.Country;
import com.diegoramos.paymentplatform.model.DocumentType;
import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.entity.PayerEntity;

import java.time.LocalDateTime;

public class PayerSample {

    public static PayerEntity getPayerEntity() {

        return PayerEntity.builder()
                .payerId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .address("Cra 99 # 14-49, Bogotá")
                .country(Country.CO)
                .documentNumber("12312312312")
                .documentType(DocumentType.CC)
                .email("john.doe@email.com")
                .phoneNumber("+57321321321")
                .build();
    }

    public static PayerDto getPayerDto() {
        return PayerDto.builder()
                .payerId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .address("Cra 99 # 14-49, Bogotá")
                .country(Country.CO)
                .documentNumber("12312312312")
                .documentType(DocumentType.CC)
                .email("john.doe@email.com")
                .phoneNumber("+57321321321")
                .build();
    }

}
