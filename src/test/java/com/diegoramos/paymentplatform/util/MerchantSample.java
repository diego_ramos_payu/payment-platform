package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.Country;
import com.diegoramos.paymentplatform.model.dto.MerchantDto;
import com.diegoramos.paymentplatform.model.entity.MerchantEntity;

import java.time.LocalDateTime;
import java.util.List;

public class MerchantSample {

    public static MerchantEntity getMerchantEntity() {

        return MerchantEntity.builder()
                .merchantId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .country(Country.CO)
                .accounts(List.of(AccountSample.getAccountEntity()))
                .address("Cra 99 # 14-49, Bogotá")
                .email("johhn.doe@email.com")
                .phoneNumber("+5721321321")
                .build();
    }

    public static MerchantDto getMerchantDto() {
        return MerchantDto.builder()
                .merchantId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .country(Country.CO)
                .accounts(List.of(AccountSample.getAccountDto()))
                .address("Cra 99 # 14-49, Bogotá")
                .email("johhn.doe@email.com")
                .phoneNumber("+5721321321")
                .build();
    }

}
