package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.dto.CreditCardDto;
import com.diegoramos.paymentplatform.model.entity.CreditCardEntity;

import java.time.LocalDateTime;

public class CreditCardSample {

    public static CreditCardEntity getSecureCreditCardEntity() {

        return CreditCardEntity.builder()
                .maskedNumber("4037********1984")
                .franchise("VISA")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .cardHolderName("John Doe")
                .expirationDate("2023/06")
                .creditCardId("15655e19-2552-4429-a307-7a1c49e01a64")
                .build();
    }

    public static CreditCardDto getUnmaskedCreditCardDto() {
        return CreditCardDto.builder()
                .number("4037997623271984")
                .franchise("VISA")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .cardHolderName("John Doe")
                .expirationDate("2023/06")
                .creditCardId("15655e19-2552-4429-a307-7a1c49e01a64")
                .cvv2("555")
                .build();
    }

    public static CreditCardDto getSecureCreditCardDto() {
        return  CreditCardDto.builder()
                .number("4037********1984")
                .cvv2("***")
                .franchise("VISA")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .cardHolderName("John Doe")
                .expirationDate("2023/06")
                .creditCardId("15655e19-2552-4429-a307-7a1c49e01a64")
                .build();
    }

}
