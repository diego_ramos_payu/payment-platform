package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.Country;
import com.diegoramos.paymentplatform.model.dto.AccountDto;
import com.diegoramos.paymentplatform.model.entity.AccountEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AccountSample {

    public static AccountEntity getAccountEntity() {

        return AccountEntity.builder()
                .accountBalance(1000.0)
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .country(Country.CO)
                .commissionRate(0.1)
                .orders(List.of(OrderSample.getOrderEntity()))
                .accountId("15655e19-2552-4429-a307-7a1c49e01a64")
                .build();
    }

    public static AccountDto getAccountDto() {
        return AccountDto.builder()
                .accountBalance(1000.0)
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .country(Country.CO)
                .commissionRate(0.1)
                .orders(new ArrayList<>(List.of(OrderSample.getCompletedOrderDto())))
                .accountId("15655e19-2552-4429-a307-7a1c49e01a64")
                .build();
    }

}
