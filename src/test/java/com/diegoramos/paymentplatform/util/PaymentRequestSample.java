package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.Currency;
import com.diegoramos.paymentplatform.request.PaymentRequest;

public class PaymentRequestSample {

    public static PaymentRequest getPaymentRequest() {

        return PaymentRequest.builder()
                .accountId("15655e19-2552-4429-a307-7a1c49e01a64")
                .payer(PayerSample.getPayerDto())
                .creditCard(CreditCardSample.getUnmaskedCreditCardDto())
                .currency(Currency.COP)
                .purchaseValue(10000.0)
                .taxValue(2000.0)
                .build();

    }

}
