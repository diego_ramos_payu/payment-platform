package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.Currency;
import com.diegoramos.paymentplatform.request.RefundRequest;

public class RefundRequestSample {

    public static RefundRequest getValidRefundRequest() {

        return RefundRequest.builder()
                .orderId("15655e19-2552-4429-a307-7a1c49e01a64")
                .refundValue(12000.0)
                .currency(Currency.COP)
                .build();
    }

    public static RefundRequest getValidPartialRefundRequest() {

        return RefundRequest.builder()
                .orderId("15655e19-2552-4429-a307-7a1c49e01a64")
                .refundValue(12000.0)
                .build();
    }

    public static RefundRequest getInvalidIdRefundRequest() {

        return RefundRequest.builder()
                .orderId("15655e19-2552-4429-a307-7a1c49e01a69")
                .refundValue(12000.0)
                .currency(Currency.COP)
                .build();
    }

    public static RefundRequest getGreaterValueRefundRequest() {

        return RefundRequest.builder()
                .orderId("15655e19-2552-4429-a307-7a1c49e01a64")
                .refundValue(13000.0)
                .currency(Currency.COP)
                .build();
    }
}
