package com.diegoramos.paymentplatform.util;

import com.diegoramos.paymentplatform.model.ProcessingStatus;
import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.entity.OrderEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OrderSample {

    public static OrderEntity getOrderEntity() {

        return OrderEntity.builder()
                .orderId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .payer(PayerSample.getPayerEntity())
                .transactions(List.of(TransactionSample.getCompletedApprovedTransactionEntity()))
                .status(ProcessingStatus.COMPLETED)
                .build();
    }

    public static OrderDto getCompletedOrderDto() {
        return OrderDto.builder()
                .orderId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .payer(PayerSample.getPayerDto())
                .transactions(new ArrayList<>(List.of(TransactionSample.getCompletedApprovedTransactionDto())))
                .status(ProcessingStatus.COMPLETED)
                .build();
    }

    public static OrderDto getInitialOrderDto() {
        return OrderDto.builder()
                .orderId("15655e19-2552-4429-a307-7a1c49e01a64")
                .creationDate(LocalDateTime.of(2022, 2, 5, 16, 0))
                .payer(PayerSample.getPayerDto())
                .transactions(List.of(TransactionSample.getTransactionDtoBaseBuilder().processingStatus(ProcessingStatus.IN_PROGRESS).build()))
                .status(ProcessingStatus.IN_PROGRESS)
                .build();
    }

}
