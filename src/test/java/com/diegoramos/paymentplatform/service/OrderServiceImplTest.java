package com.diegoramos.paymentplatform.service;

import com.diegoramos.paymentplatform.mapper.CreditCardMapper;
import com.diegoramos.paymentplatform.mapper.OrderMapper;
import com.diegoramos.paymentplatform.mapper.PayerMapper;
import com.diegoramos.paymentplatform.mapper.TransactionMapper;
import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.model.entity.OrderEntity;
import com.diegoramos.paymentplatform.repository.OrderRepository;
import com.diegoramos.paymentplatform.service.api.OrderService;
import com.diegoramos.paymentplatform.service.impl.OrderServiceImpl;
import com.diegoramos.paymentplatform.util.OrderSample;
import com.diegoramos.paymentplatform.util.TransactionSample;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

public class OrderServiceImplTest {


    @MockBean
    OrderRepository orderRepository;

    OrderService orderService;

    @BeforeEach
    void SetUp() {

        orderRepository = Mockito.mock(OrderRepository.class);
        orderService = new OrderServiceImpl(orderRepository, new OrderMapper(new PayerMapper(), new TransactionMapper(new CreditCardMapper())));
    }

    @Test
    void persistOrderTest() {

        final OrderDto orderDto = OrderSample.getCompletedOrderDto();
        final OrderDto expectedOrderDto = OrderSample.getCompletedOrderDto();

        Mockito.when(orderRepository.save(Mockito.any(OrderEntity.class))).thenReturn(OrderSample.getOrderEntity());

        final OrderDto actualOrder = orderService.persistOrder(orderDto);

        Assertions.assertThat(actualOrder).usingRecursiveComparison().isEqualTo(expectedOrderDto);

    }

    @Test
    void updateOrderTest() {

        final OrderDto orderDto = OrderSample.getCompletedOrderDto();
        final OrderDto expectedOrderDto = OrderSample.getCompletedOrderDto();

        Mockito.when(orderRepository.save(Mockito.any(OrderEntity.class))).thenReturn(OrderSample.getOrderEntity());

        final OrderDto actualOrder = orderService.updateOrder(orderDto);

        Assertions.assertThat(actualOrder).usingRecursiveComparison().isEqualTo(expectedOrderDto);

    }

    @Test
    void getOrderTest() {

        final OrderDto expectedOrderDto = OrderSample.getCompletedOrderDto();
        final String orderId = expectedOrderDto.getOrderId();

        Mockito.when(orderRepository.findById(Mockito.any(String.class))).thenReturn(Optional.of(OrderSample.getOrderEntity()));

        final OrderDto actualOrder = orderService.getOrder(orderId);

        Assertions.assertThat(actualOrder).usingRecursiveComparison().isEqualTo(expectedOrderDto);
    }

    @Test
    void getApprovedAuthorizationAndCaptureTransactionTest_whenAuthorizationAndCaptureTransactionPresent_shouldReturnTransaction() {

        final TransactionDto expectedTransaction = TransactionSample.getCompletedApprovedTransactionDto();
        final OrderDto orderDto = OrderSample.getCompletedOrderDto();

        final TransactionDto actualTransaction = orderService.getApprovedAuthorizationAndCaptureTransaction(orderDto);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedTransaction);

    }

    @Test
    void getApprovedAuthorizationAndCaptureTransactionTest_whenNoTransactionApproved_shouldThrowException() {

        final OrderDto orderDto = OrderSample.getInitialOrderDto();
        final EntityNotFoundException expectedException = new EntityNotFoundException(
                String.format("Order with id %s does not have an approved authorization and capture transaction",
                        orderDto.getOrderId()));

        Assertions.assertThatThrownBy(() -> {
            orderService.getApprovedAuthorizationAndCaptureTransaction(orderDto);
        }).isInstanceOf(EntityNotFoundException.class).hasMessage(expectedException.getMessage());

    }

}
