package com.diegoramos.paymentplatform.service;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.request.PaymentRequest;
import com.diegoramos.paymentplatform.request.RefundRequest;
import com.diegoramos.paymentplatform.service.api.*;
import com.diegoramos.paymentplatform.service.impl.PaymentsServiceImpl;
import com.diegoramos.paymentplatform.util.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;

public class PaymentsServiceImplTest {

    @MockBean
    BankService bankService;

    @MockBean
    OrderService orderService;

    @MockBean
    TransactionService transactionService;

    @MockBean
    AntifraudService antifraudService;

    @MockBean
    AccountService accountService;


    PaymentsService paymentsService;

    @BeforeEach
    void setUp() {

        bankService = Mockito.mock(BankService.class);
        antifraudService = Mockito.mock(AntifraudService.class);
        orderService = Mockito.mock(OrderService.class);
        transactionService = Mockito.mock(TransactionService.class);
        accountService = Mockito.mock(AccountService.class);
        paymentsService = new PaymentsServiceImpl(bankService, orderService, transactionService, accountService, antifraudService);
    }


    @Test
    void submitCreditCardPaymentTest_whenAntifraudAndBankSuccess_shouldReturnApprovedTransaction() {

        final PaymentRequest paymentRequest = PaymentRequestSample.getPaymentRequest();
        final TransactionDto expectedTransaction = TransactionSample.getCompletedApprovedTransactionDto();

        Mockito.when(accountService.getAccount(Mockito.any(String.class))).thenReturn(AccountSample.getAccountDto());
        Mockito.when(orderService.persistOrder(Mockito.any(OrderDto.class))).thenReturn(OrderSample.getInitialOrderDto());
        Mockito.when(antifraudService.evaluateTransaction(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getAntifraudAcceptedTransactionDto());
        Mockito.when(bankService.submitPaymentToBank(Mockito.any(TransactionDto.class), Mockito.any(PayerDto.class)))
                .thenReturn(TransactionSample.getBankAcceptedTransactionDto());

        final TransactionDto actualTransaction = paymentsService.submitCreditCardPayment(paymentRequest);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedTransaction);

    }


    @Test
    void submitCreditCardPaymentTest_whenAntifraudDeclines_shouldReturnDeclinedTransaction() {

        final PaymentRequest paymentRequest = PaymentRequestSample.getPaymentRequest();
        final TransactionDto expectedTransaction = TransactionSample.getCompletedAntifraudDeclinedTransactionDto();

        Mockito.when(orderService.persistOrder(Mockito.any(OrderDto.class))).thenReturn(OrderSample.getInitialOrderDto());
        Mockito.when(antifraudService.evaluateTransaction(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getAntifraudDeclinedTransactionDto());
        Mockito.when(accountService.getAccount(Mockito.any(String.class))).thenReturn(AccountSample.getAccountDto());

        final TransactionDto actualTransaction = paymentsService.submitCreditCardPayment(paymentRequest);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedTransaction);

    }


    @Test
    void submitCreditCardPaymentTest_whenBankSubmitDeclines_shouldReturnDeclinedTransaction() {

        final PaymentRequest paymentRequest = PaymentRequestSample.getPaymentRequest();
        final TransactionDto expectedTransaction = TransactionSample.getCompletedBankDeclinedTransactionDto();

        Mockito.when(orderService.persistOrder(Mockito.any(OrderDto.class))).thenReturn(OrderSample.getInitialOrderDto());
        Mockito.when(antifraudService.evaluateTransaction(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getAntifraudAcceptedTransactionDto());
        Mockito.when(bankService.submitPaymentToBank(Mockito.any(TransactionDto.class), Mockito.any(PayerDto.class)))
                .thenReturn(TransactionSample.getBankDeclinedTransactionDto());
        Mockito.when(accountService.getAccount(Mockito.any(String.class))).thenReturn(AccountSample.getAccountDto());

        final TransactionDto actualTransaction = paymentsService.submitCreditCardPayment(paymentRequest);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedTransaction);

    }

    @Test
    void refundCreditCardPaymentTest_whenBankAccepts_shouldReturnAcceptedRefundTransaction() {

        final RefundRequest refundRequest = RefundRequestSample.getValidRefundRequest();
        final TransactionDto expectedRefund = TransactionSample.getCompletedAcceptedRefundTransactionDto();

        Mockito.when(orderService.getOrder(Mockito.any(String.class))).thenReturn(OrderSample.getCompletedOrderDto());
        Mockito.when(orderService.getApprovedAuthorizationAndCaptureTransaction(Mockito.any(OrderDto.class)))
                .thenReturn(TransactionSample.getCompletedApprovedTransactionDto());
        Mockito.when(bankService.submitRefundToBank(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getAcceptedRefundTransactionDto());
        Mockito.when(transactionService.updateTransaction(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getCompletedAcceptedRefundTransactionDto());

        final TransactionDto actualTransaction = paymentsService.submitRefund(refundRequest);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedRefund);
    }

    @Test
    void refundCreditCardPaymentTest_whenBankDeclines_shouldReturnDeclinedRefundTransaction() {

        final RefundRequest refundRequest = RefundRequestSample.getValidRefundRequest();
        final TransactionDto expectedRefund = TransactionSample.getCompletedDeclinedRefundTransactionDto();

        Mockito.when(orderService.getOrder(Mockito.any(String.class))).thenReturn(OrderSample.getCompletedOrderDto());
        Mockito.when(orderService.getApprovedAuthorizationAndCaptureTransaction(Mockito.any(OrderDto.class)))
                .thenReturn(TransactionSample.getCompletedApprovedTransactionDto());
        Mockito.when(bankService.submitRefundToBank(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getDeclinedRefundTransactionDto());
        Mockito.when(transactionService.updateTransaction(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getCompletedDeclinedRefundTransactionDto());

        final TransactionDto actualTransaction = paymentsService.submitRefund(refundRequest);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedRefund);
    }

    @Test
    void refundCreditCardPaymentTest_whenPartialRefund_shouldReturnAcceptedRefundTransaction() {

        final RefundRequest refundRequest = RefundRequestSample.getValidPartialRefundRequest();
        final TransactionDto expectedRefund = TransactionSample.getCompletedAcceptedPartialRefundTransactionDto();

        Mockito.when(orderService.getOrder(Mockito.any(String.class))).thenReturn(OrderSample.getCompletedOrderDto());
        Mockito.when(orderService.getApprovedAuthorizationAndCaptureTransaction(Mockito.any(OrderDto.class)))
                .thenReturn(TransactionSample.getCompletedApprovedTransactionDto());
        Mockito.when(bankService.submitRefundToBank(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getAcceptedPartialRefundTransactionDto());
        Mockito.when(transactionService.updateTransaction(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getCompletedAcceptedPartialRefundTransactionDto());

        final TransactionDto actualTransaction = paymentsService.submitRefund(refundRequest);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedRefund);
    }

    @Test
    void refundCreditCardPaymentTest_whenInvalidRefund_shouldReturnDeclinedRefundTransaction() {

        final RefundRequest refundRequest = RefundRequestSample.getGreaterValueRefundRequest();
        final TransactionDto expectedRefund = TransactionSample.getCompletedDeclinedInvalidValueRefundTransactionDto();

        Mockito.when(orderService.getOrder(Mockito.any(String.class))).thenReturn(OrderSample.getCompletedOrderDto());
        Mockito.when(orderService.getApprovedAuthorizationAndCaptureTransaction(Mockito.any(OrderDto.class)))
                .thenReturn(TransactionSample.getCompletedApprovedTransactionDto());
        Mockito.when(transactionService.updateTransaction(Mockito.any(TransactionDto.class)))
                .thenReturn(TransactionSample.getCompletedDeclinedInvalidValueRefundTransactionDto());

        final TransactionDto actualTransaction = paymentsService.submitRefund(refundRequest);

        Assertions.assertThat(actualTransaction).usingRecursiveComparison().isEqualTo(expectedRefund);
    }

}
