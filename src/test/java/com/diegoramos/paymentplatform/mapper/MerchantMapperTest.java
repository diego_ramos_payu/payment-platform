package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.MerchantDto;
import com.diegoramos.paymentplatform.model.entity.MerchantEntity;
import com.diegoramos.paymentplatform.util.MerchantSample;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MerchantMapperTest {

    MerchantMapper merchantMapper;

    @BeforeEach
    void SetUp() {

        merchantMapper = new MerchantMapper(new AccountMapper(new OrderMapper(new PayerMapper(), new TransactionMapper(new CreditCardMapper()))));

    }

    @Test
    void getDtoTest() {

        MerchantEntity merchantEntity = MerchantSample.getMerchantEntity();
        MerchantDto expectedMerchantDto = MerchantSample.getMerchantDto();

        MerchantDto actualDto = merchantMapper.getDto(merchantEntity);

        Assertions.assertThat(expectedMerchantDto).usingRecursiveComparison().isEqualTo(actualDto);
    }

    @Test
    void getEntityTest() {

        MerchantDto merchantDto = MerchantSample.getMerchantDto();
        MerchantEntity expectedMerchantEntity = MerchantSample.getMerchantEntity();

        MerchantEntity actualEntity = merchantMapper.getEntity(merchantDto);

        Assertions.assertThat(expectedMerchantEntity).usingRecursiveComparison().isEqualTo(actualEntity);
    }
}
