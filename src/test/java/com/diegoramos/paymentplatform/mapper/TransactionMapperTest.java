package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.model.entity.TransactionEntity;
import com.diegoramos.paymentplatform.util.TransactionSample;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TransactionMapperTest {

    TransactionMapper transactionMapper;

    @BeforeEach
    void SetUp() {

        transactionMapper = new TransactionMapper(new CreditCardMapper());

    }

    @Test
    void getDtoTest() {

        TransactionEntity transactionEntity = TransactionSample.getCompletedApprovedTransactionEntity();
        TransactionDto expectedTransactionDto = TransactionSample.getCompletedApprovedTransactionDto();

        TransactionDto actualDto = transactionMapper.getDto(transactionEntity);

        Assertions.assertThat(actualDto).usingRecursiveComparison().isEqualTo(expectedTransactionDto);

    }

    @Test
    void getEntityTest() {

        TransactionDto transactionDto = TransactionSample.getCompletedApprovedTransactionDto();
        TransactionEntity expectedTransactionEntity = TransactionSample.getCompletedApprovedTransactionEntity();

        TransactionEntity actualEntity = transactionMapper.getEntity(transactionDto);

        Assertions.assertThat(actualEntity).usingRecursiveComparison().isEqualTo(expectedTransactionEntity);

    }
}
