package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.entity.PayerEntity;
import com.diegoramos.paymentplatform.util.PayerSample;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PayerMapperTest {

    PayerMapper payerMapper;

    @BeforeEach
    void SetUp() {

        payerMapper = new PayerMapper();

    }

    @Test
    void getDtoTest() {

        PayerEntity payerEntity = PayerSample.getPayerEntity();
        PayerDto expectedPayerDto = PayerSample.getPayerDto();

        PayerDto actualDto = payerMapper.getDto(payerEntity);

        Assertions.assertThat(expectedPayerDto).usingRecursiveComparison().isEqualTo(actualDto);

    }

    @Test
    void getEntityTest() {

        PayerDto payerDto = PayerSample.getPayerDto();
        PayerEntity expectedPayerEntity = PayerSample.getPayerEntity();

        PayerEntity actualEntity = payerMapper.getEntity(payerDto);

        Assertions.assertThat(expectedPayerEntity).usingRecursiveComparison().isEqualTo(actualEntity);

    }

}
