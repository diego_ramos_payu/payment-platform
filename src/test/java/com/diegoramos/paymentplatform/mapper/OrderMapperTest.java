package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.entity.OrderEntity;
import com.diegoramos.paymentplatform.util.OrderSample;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderMapperTest {

    OrderMapper orderMapper;

    @BeforeEach
    void setUp() {

        orderMapper = new OrderMapper(new PayerMapper(), new TransactionMapper(new CreditCardMapper()));

    }

    @Test
    void getDtoTest() {

        OrderEntity orderEntity = OrderSample.getOrderEntity();
        OrderDto expectedOrderDto = OrderSample.getCompletedOrderDto();

        OrderDto actualDto = orderMapper.getDto(orderEntity);

        Assertions.assertThat(expectedOrderDto).usingRecursiveComparison().isEqualTo(actualDto);
    }

    @Test
    void getEntityTest() {

        OrderDto orderDto = OrderSample.getCompletedOrderDto();
        OrderEntity expectedOrderEntity = OrderSample.getOrderEntity();

        OrderEntity actualEntity = orderMapper.getEntity(orderDto);

        Assertions.assertThat(expectedOrderEntity).usingRecursiveComparison().isEqualTo(actualEntity);
    }
}
