package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.CreditCardDto;
import com.diegoramos.paymentplatform.model.entity.CreditCardEntity;
import com.diegoramos.paymentplatform.util.CreditCardSample;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CreditCardMapperTest {

    CreditCardMapper creditCardMapper;

    @BeforeEach
    void SetUp() {

        creditCardMapper = new CreditCardMapper();

    }

    @Test
    void getDtoTest() {

        CreditCardEntity creditCardEntity = CreditCardSample.getSecureCreditCardEntity();
        CreditCardDto expectedCreditCardDto = CreditCardSample.getSecureCreditCardDto();

        CreditCardDto actualDto = creditCardMapper.getDto(creditCardEntity);

        Assertions.assertThat(expectedCreditCardDto).usingRecursiveComparison().isEqualTo(actualDto);

    }

    @Test
    void getEntityTest() {

        CreditCardDto creditCardDto = CreditCardSample.getUnmaskedCreditCardDto();
        CreditCardEntity expectedCreditCardEntity = CreditCardSample.getSecureCreditCardEntity();

        CreditCardEntity actualEntity = creditCardMapper.getEntity(creditCardDto);

        Assertions.assertThat(expectedCreditCardEntity).usingRecursiveComparison().isEqualTo(actualEntity);

    }

    @Test
    void maskCreditCardNumberTest() {

        String creditCardNumber = CreditCardSample.getUnmaskedCreditCardDto().getNumber();
        String expectedMaskedNumber = CreditCardSample.getSecureCreditCardEntity().getMaskedNumber();

        String actualMaskedNumber = creditCardMapper.maskCreditCardNumber(creditCardNumber);

        Assertions.assertThat(expectedMaskedNumber).isEqualTo(actualMaskedNumber);
    }

}
