package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.AccountDto;
import com.diegoramos.paymentplatform.model.entity.AccountEntity;
import com.diegoramos.paymentplatform.util.AccountSample;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AccountMapperTest {

    AccountMapper accountMapper;

    @BeforeEach
    void SetUp() {

        accountMapper = new AccountMapper(new OrderMapper(new PayerMapper(), new TransactionMapper(new CreditCardMapper())));

    }

    @Test
    void getDtoTest() {

        AccountEntity accountEntity = AccountSample.getAccountEntity();
        AccountDto expectedAccountDto = AccountSample.getAccountDto();

        AccountDto actualDto = accountMapper.getDto(accountEntity);

        Assertions.assertThat(expectedAccountDto).usingRecursiveComparison().isEqualTo(actualDto);
    }

    @Test
    void getEntityTest() {

        AccountDto accountDto = AccountSample.getAccountDto();
        AccountEntity expectedAccountEntity = AccountSample.getAccountEntity();

        AccountEntity actualEntity = accountMapper.getEntity(accountDto);

        Assertions.assertThat(expectedAccountEntity).usingRecursiveComparison().isEqualTo(actualEntity);

    }
}
