package com.diegoramos.paymentplatform.model;

public enum ProcessingStatus {
    IN_PROGRESS,
    COMPLETED
}
