package com.diegoramos.paymentplatform.model.dto;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class CreditCardDto {

    private final String creditCardId;

    private final LocalDateTime creationDate;

    @NotEmpty
    private final String expirationDate;

    @NotEmpty
    private final String number;

    @NotEmpty
    private final String cvv2;

    @NotEmpty
    private final String cardHolderName;

    @NotEmpty
    private final String franchise;
}
