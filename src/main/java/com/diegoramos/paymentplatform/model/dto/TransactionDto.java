package com.diegoramos.paymentplatform.model.dto;

import com.diegoramos.paymentplatform.model.*;
import com.diegoramos.paymentplatform.model.entity.BankStatus;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class TransactionDto {

    private String transactionId;

    private String orderId;

    private LocalDateTime creationDate;

    private TransactionType type;

    private ProcessingStatus processingStatus;

    private ApprovalStatus approvalStatus;

    private AntifraudStatus antifraudStatus;

    private BankStatus bankStatus;

    private Double value;

    private Double taxValue;

    private Currency currency;

    private Double networkValue;

    private CreditCardDto creditCard;

    private String creditCardToken;

    private String errorMessage;
}
