package com.diegoramos.paymentplatform.model.dto;

import com.diegoramos.paymentplatform.model.Country;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class AccountDto {

    private String accountId;

    private LocalDateTime creationDate;

    private Country country;

    private Double accountBalance;

    private Double commissionRate;

    private List<OrderDto> orders;
}
