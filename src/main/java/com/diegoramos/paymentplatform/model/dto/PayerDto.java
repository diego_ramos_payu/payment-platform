package com.diegoramos.paymentplatform.model.dto;

import com.diegoramos.paymentplatform.model.Country;
import com.diegoramos.paymentplatform.model.DocumentType;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class PayerDto {

    private String payerId;

    @NotEmpty
    private String documentNumber;

    private DocumentType documentType;

    private LocalDateTime creationDate;

    @NotEmpty
    private String name;

    @NotEmpty
    private String address;

    private Country country;

    @NotEmpty
    private String email;

    @NotEmpty
    private String phoneNumber;
}
