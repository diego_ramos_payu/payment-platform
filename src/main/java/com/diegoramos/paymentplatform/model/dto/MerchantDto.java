package com.diegoramos.paymentplatform.model.dto;

import com.diegoramos.paymentplatform.model.Country;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class MerchantDto {

    private String merchantId;

    private LocalDateTime creationDate;

    private String name;

    private String address;

    private Country country;

    private String email;

    private String phoneNumber;

    private List<AccountDto> accounts;
}
