package com.diegoramos.paymentplatform.model.dto;

import com.diegoramos.paymentplatform.model.ProcessingStatus;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OrderDto {

    private String orderId;

    private LocalDateTime creationDate;

    private ProcessingStatus status;

    private PayerDto payer;

    private List<TransactionDto> transactions;
}
