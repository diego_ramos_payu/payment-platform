package com.diegoramos.paymentplatform.model.entity;

public enum BankStatus {
    BANK_DECLINED,
    BANK_ACCEPTED
}
