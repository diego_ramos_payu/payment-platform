package com.diegoramos.paymentplatform.model.entity;

import com.diegoramos.paymentplatform.model.Country;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "accounts")
public class AccountEntity {

    @Id
    private String accountId;

    private LocalDateTime creationDate;

    @Enumerated(EnumType.STRING)
    private Country country;

    private Double accountBalance;

    private Double commissionRate;

    @OneToMany
    private List<OrderEntity> orders;
}
