package com.diegoramos.paymentplatform.model.entity;

import com.diegoramos.paymentplatform.model.Country;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "merchants")
public class MerchantEntity {

    @Id
    private String merchantId;

    private LocalDateTime creationDate;

    private String name;

    private String address;

    @Enumerated(EnumType.STRING)
    private Country country;

    private String email;

    private String phoneNumber;

    @OneToMany
    private List<AccountEntity> accounts;

}
