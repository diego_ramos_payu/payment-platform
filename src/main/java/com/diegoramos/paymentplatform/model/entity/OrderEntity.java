package com.diegoramos.paymentplatform.model.entity;

import com.diegoramos.paymentplatform.model.ProcessingStatus;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class OrderEntity {

    @Id
    private String orderId;

    private LocalDateTime creationDate;

    @Enumerated(EnumType.STRING)
    private ProcessingStatus status;

    @OneToOne(cascade = CascadeType.ALL)
    private PayerEntity payer;

    @OneToMany(cascade = CascadeType.ALL)
    private List<TransactionEntity> transactions;
}
