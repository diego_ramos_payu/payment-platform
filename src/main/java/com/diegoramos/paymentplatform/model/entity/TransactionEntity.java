package com.diegoramos.paymentplatform.model.entity;

import com.diegoramos.paymentplatform.model.*;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "transactions")
public class TransactionEntity {

    @Id
    private String transactionId;

    private String orderId;

    private LocalDateTime creationDate;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Enumerated(EnumType.STRING)
    private ProcessingStatus processingStatus;

    @Enumerated(EnumType.STRING)
    private ApprovalStatus approvalStatus;

    @Enumerated(EnumType.STRING)
    private AntifraudStatus antifraudStatus;

    @Enumerated(EnumType.STRING)
    private BankStatus bankStatus;

    private Double value;

    private Double taxValue;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    private Double networkValue;

    @OneToOne(cascade = CascadeType.ALL)
    private CreditCardEntity creditCard;

    private String creditCardToken;

    private String errorMessage;
}
