package com.diegoramos.paymentplatform.model.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "credit_cards")
public class CreditCardEntity {

    @Id
    private String creditCardId;

    private LocalDateTime creationDate;

    private String expirationDate;

    private String maskedNumber;

    private String cardHolderName;

    private String franchise;
}
