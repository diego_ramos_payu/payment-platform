package com.diegoramos.paymentplatform.model.entity;

import com.diegoramos.paymentplatform.model.Country;
import com.diegoramos.paymentplatform.model.DocumentType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "payers")
public class PayerEntity {

    @Id
    private String payerId;

    private String documentNumber;

    @Enumerated(EnumType.STRING)
    private DocumentType documentType;

    private LocalDateTime creationDate;

    private String name;

    private String address;

    @Enumerated(EnumType.STRING)
    private Country country;

    private String email;

    private String phoneNumber;
}
