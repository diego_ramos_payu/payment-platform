package com.diegoramos.paymentplatform.model;

public enum TransactionType {
    AUTHORIZATION_AND_CAPTURE,
    REFUND
}
