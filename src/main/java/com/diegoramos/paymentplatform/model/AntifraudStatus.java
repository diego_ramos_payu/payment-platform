package com.diegoramos.paymentplatform.model;

public enum AntifraudStatus {
    ANTIFRAUD_DECLINED,
    ANTIFRAUD_ACCEPTED
}
