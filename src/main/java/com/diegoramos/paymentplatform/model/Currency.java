package com.diegoramos.paymentplatform.model;

public enum Currency {
    COP,
    USD
}
