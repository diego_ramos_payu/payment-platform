package com.diegoramos.paymentplatform.model;

public enum ApprovalStatus {
    APPROVED,
    DECLINED
}
