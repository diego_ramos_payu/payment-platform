package com.diegoramos.paymentplatform.response;

import com.diegoramos.paymentplatform.model.ApprovalStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PaymentResponse {

    private final String orderId;

    private final String transactionId;

    private final ApprovalStatus approvalStatus;

    private final String errorMessage;
}
