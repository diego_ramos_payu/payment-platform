package com.diegoramos.paymentplatform.response;

import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RefundResponse {

    private final TransactionDto transactionDto;

    private final String errorMessage;
}
