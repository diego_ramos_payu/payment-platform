package com.diegoramos.paymentplatform.response;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class QueryPaymentResponse {

    private final OrderDto order;

    private final String errorMessage;

}
