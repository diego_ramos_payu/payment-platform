package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.model.entity.OrderEntity;
import com.diegoramos.paymentplatform.model.entity.TransactionEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class TransactionMapper implements Mapper<TransactionDto, TransactionEntity>, ListMapper<TransactionDto, TransactionEntity> {

    @Autowired
    private CreditCardMapper creditCardMapper;

    @Override
    public TransactionDto getDto(TransactionEntity transactionEntity) {

        return TransactionDto.builder()
                .transactionId(transactionEntity.getTransactionId())
                .orderId(transactionEntity.getOrderId())
                .creationDate(transactionEntity.getCreationDate())
                .currency(transactionEntity.getCurrency())
                .type(transactionEntity.getType())
                .value(transactionEntity.getValue())
                .taxValue(transactionEntity.getTaxValue())
                .networkValue(transactionEntity.getNetworkValue())
                .antifraudStatus(transactionEntity.getAntifraudStatus())
                .bankStatus(transactionEntity.getBankStatus())
                .processingStatus(transactionEntity.getProcessingStatus())
                .approvalStatus(transactionEntity.getApprovalStatus())
                .creditCard(transactionEntity.getCreditCard() != null ? creditCardMapper.getDto(transactionEntity.getCreditCard()) : null)
                .creditCardToken(transactionEntity.getCreditCardToken())
                .errorMessage(transactionEntity.getErrorMessage())
                .build();
    }

    @Override
    public TransactionEntity getEntity(TransactionDto transactionDto) {

        return TransactionEntity.builder()
                .transactionId(transactionDto.getTransactionId())
                .orderId(transactionDto.getOrderId())
                .creationDate(transactionDto.getCreationDate())
                .currency(transactionDto.getCurrency())
                .type(transactionDto.getType())
                .value(transactionDto.getValue())
                .taxValue(transactionDto.getTaxValue())
                .networkValue(transactionDto.getNetworkValue())
                .antifraudStatus(transactionDto.getAntifraudStatus())
                .bankStatus(transactionDto.getBankStatus())
                .processingStatus(transactionDto.getProcessingStatus())
                .approvalStatus(transactionDto.getApprovalStatus())
                .creditCard(transactionDto.getCreditCard() != null ? creditCardMapper.getEntity(transactionDto.getCreditCard()) : null)
                .creditCardToken(transactionDto.getCreditCardToken())
                .errorMessage(transactionDto.getErrorMessage())
                .build();
    }

    @Override
    public List<TransactionDto> getDtoList(List<TransactionEntity> transactionEntities) {

        List<TransactionDto> dtos = new ArrayList<>();

        for (TransactionEntity transactionEntity: transactionEntities) {

            dtos.add(getDto(transactionEntity));
        }

        return dtos;
    }

    @Override
    public List<TransactionEntity> getEntityList(List<TransactionDto> transactionDtos) {

        List<TransactionEntity> entities = new ArrayList<>();

        for (TransactionDto transactionDto: transactionDtos) {

            entities.add(getEntity(transactionDto));
        }

        return entities;
    }
}
