package com.diegoramos.paymentplatform.mapper;

import java.util.List;

public interface ListMapper<Dto, Entity>{

    List<Dto> getDtoList(final List<Entity> entities);

    List<Entity> getEntityList(final List<Dto> dtos);
}
