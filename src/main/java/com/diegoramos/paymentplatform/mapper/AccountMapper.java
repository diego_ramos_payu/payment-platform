package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.AccountDto;
import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.entity.AccountEntity;
import com.diegoramos.paymentplatform.model.entity.OrderEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class AccountMapper implements Mapper<AccountDto, AccountEntity>, ListMapper<AccountDto, AccountEntity> {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public AccountDto getDto(AccountEntity accountEntity) {
        return AccountDto.builder()
                .accountId(accountEntity.getAccountId())
                .creationDate(accountEntity.getCreationDate())
                .accountBalance(accountEntity.getAccountBalance())
                .country(accountEntity.getCountry())
                .commissionRate(accountEntity.getCommissionRate())
                .orders(orderMapper.getDtoList(accountEntity.getOrders()))
                .build();
    }

    @Override
    public AccountEntity getEntity(AccountDto accountDto) {

        return AccountEntity.builder()
                .accountId(accountDto.getAccountId())
                .creationDate(accountDto.getCreationDate())
                .accountBalance(accountDto.getAccountBalance())
                .country(accountDto.getCountry())
                .commissionRate(accountDto.getCommissionRate())
                .orders(orderMapper.getEntityList(accountDto.getOrders()))
                .build();
    }

    @Override
    public List<AccountDto> getDtoList(List<AccountEntity> accountEntities) {

        List<AccountDto> dtos = new ArrayList<>();

        for (AccountEntity accountEntity : accountEntities) {

            dtos.add(getDto(accountEntity));
        }

        return dtos;
    }

    @Override
    public List<AccountEntity> getEntityList(List<AccountDto> accountDtos) {

        List<AccountEntity> entities = new ArrayList<>();

        for (AccountDto accountDto : accountDtos) {

            entities.add(getEntity(accountDto));
        }

        return entities;
    }
}
