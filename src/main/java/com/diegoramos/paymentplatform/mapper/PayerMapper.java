package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.entity.PayerEntity;
import org.springframework.stereotype.Component;

@Component
public class PayerMapper implements Mapper<PayerDto, PayerEntity> {

    @Override
    public PayerDto getDto(PayerEntity payerEntity) {

        return PayerDto.builder()
                .payerId(payerEntity.getPayerId())
                .creationDate(payerEntity.getCreationDate())
                .address(payerEntity.getAddress())
                .country(payerEntity.getCountry())
                .documentNumber(payerEntity.getDocumentNumber())
                .documentType(payerEntity.getDocumentType())
                .email(payerEntity.getEmail())
                .phoneNumber(payerEntity.getPhoneNumber())
                .name(payerEntity.getName())
                .build();
    }

    @Override
    public PayerEntity getEntity(PayerDto payerDto) {

        return PayerEntity.builder()
                .payerId(payerDto.getPayerId())
                .creationDate(payerDto.getCreationDate())
                .address(payerDto.getAddress())
                .country(payerDto.getCountry())
                .documentNumber(payerDto.getDocumentNumber())
                .documentType(payerDto.getDocumentType())
                .email(payerDto.getEmail())
                .phoneNumber(payerDto.getPhoneNumber())
                .name(payerDto.getName())
                .build();
    }
}
