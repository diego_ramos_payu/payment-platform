package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.MerchantDto;
import com.diegoramos.paymentplatform.model.entity.MerchantEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class MerchantMapper implements Mapper<MerchantDto, MerchantEntity> {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public MerchantDto getDto(MerchantEntity merchantEntity) {

        return MerchantDto.builder()
                .merchantId(merchantEntity.getMerchantId())
                .creationDate(merchantEntity.getCreationDate())
                .address(merchantEntity.getAddress())
                .country(merchantEntity.getCountry())
                .email(merchantEntity.getEmail())
                .name(merchantEntity.getName())
                .phoneNumber(merchantEntity.getPhoneNumber())
                .accounts(accountMapper.getDtoList(merchantEntity.getAccounts()))
                .build();
    }

    @Override
    public MerchantEntity getEntity(MerchantDto merchantDto) {
        return MerchantEntity.builder()
                .merchantId(merchantDto.getMerchantId())
                .creationDate(merchantDto.getCreationDate())
                .address(merchantDto.getAddress())
                .country(merchantDto.getCountry())
                .email(merchantDto.getEmail())
                .name(merchantDto.getName())
                .phoneNumber(merchantDto.getPhoneNumber())
                .accounts(accountMapper.getEntityList(merchantDto.getAccounts()))
                .build();
    }
}
