package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.CreditCardDto;
import com.diegoramos.paymentplatform.model.entity.CreditCardEntity;
import org.springframework.stereotype.Component;

@Component
public class CreditCardMapper implements Mapper<CreditCardDto, CreditCardEntity> {

    @Override
    public CreditCardDto getDto(final CreditCardEntity creditCardEntity) {
        return CreditCardDto.builder()
                .creditCardId(creditCardEntity.getCreditCardId())
                .cardHolderName(creditCardEntity.getCardHolderName())
                .creationDate(creditCardEntity.getCreationDate())
                .franchise(creditCardEntity.getFranchise())
                .number(creditCardEntity.getMaskedNumber())
                .cvv2("***")
                .expirationDate(creditCardEntity.getExpirationDate())
                .build();
    }

    @Override
    public CreditCardEntity getEntity(final CreditCardDto creditCardDto) {
        return CreditCardEntity.builder()
                .creditCardId(creditCardDto.getCreditCardId())
                .cardHolderName(creditCardDto.getCardHolderName())
                .creationDate(creditCardDto.getCreationDate())
                .franchise(creditCardDto.getFranchise())
                .maskedNumber(maskCreditCardNumber(creditCardDto.getNumber()))
                .expirationDate(creditCardDto.getExpirationDate())
                .build();
    }

    protected String maskCreditCardNumber(final String number) {

        char[] chars = number.toCharArray();

        for (int i = 4; i < number.length() - 4 ; i++) {

            chars[i] = '*';
        }

        return new String(chars);
    }
}
