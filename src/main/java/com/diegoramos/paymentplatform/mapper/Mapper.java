package com.diegoramos.paymentplatform.mapper;

import org.springframework.stereotype.Component;

@Component
public interface Mapper<Dto, Entity>{

    Dto getDto(final Entity entity);

    Entity getEntity(final Dto dto);
}
