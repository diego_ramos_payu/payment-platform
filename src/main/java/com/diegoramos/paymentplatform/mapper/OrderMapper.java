package com.diegoramos.paymentplatform.mapper;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.entity.OrderEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class OrderMapper implements Mapper<OrderDto, OrderEntity>, ListMapper<OrderDto, OrderEntity> {

    @Autowired
    PayerMapper payerMapper;

    @Autowired
    TransactionMapper transactionMapper;

    @Override
    public OrderDto getDto(final OrderEntity orderEntity) {

        return OrderDto.builder()
                .orderId(orderEntity.getOrderId())
                .creationDate(orderEntity.getCreationDate())
                .payer(payerMapper.getDto(orderEntity.getPayer()))
                .transactions(transactionMapper.getDtoList(orderEntity.getTransactions()))
                .status(orderEntity.getStatus())
                .build();
    }

    @Override
    public OrderEntity getEntity(final OrderDto orderDto) {

        return OrderEntity.builder()
                .orderId(orderDto.getOrderId())
                .creationDate(orderDto.getCreationDate())
                .payer(payerMapper.getEntity(orderDto.getPayer()))
                .transactions(transactionMapper.getEntityList(orderDto.getTransactions()))
                .status(orderDto.getStatus())
                .build();
    }

    @Override
    public List<OrderDto> getDtoList(final List<OrderEntity> orderEntities) {

        List<OrderDto> dtos = new ArrayList<>();

        for (OrderEntity orderEntity : orderEntities) {

            dtos.add(getDto(orderEntity));
        }

        return dtos;
    }

    @Override
    public List<OrderEntity> getEntityList(final List<OrderDto> orderDtos) {

        List<OrderEntity> entities = new ArrayList<>();

        for (OrderDto orderDto: orderDtos) {

            entities.add(getEntity(orderDto));
        }

        return entities;
    }
}
