package com.diegoramos.paymentplatform;

import com.diegoramos.paymentplatform.repository.AccountRepository;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootApplication
public class PaymentPlatformApplication {

    public static void main(String[] args) {

        SpringApplication.run(PaymentPlatformApplication.class, args);
        startAntifraudMock();
        startBankMock();
    }

    private static void startAntifraudMock() {

        WireMockServer antifraudMock = new WireMockServer(8081);

		antifraudMock.stubFor(post("/validate")
                .willReturn(aResponse().withStatus(400)));

        antifraudMock.stubFor(post("/validate")
                .withRequestBody(containing("555"))
                .willReturn(aResponse().withStatus(200)));

        antifraudMock.stubFor(post("/validate")
                .withRequestBody(containing("222"))
                .willReturn(aResponse().withStatus(200)));

        antifraudMock.start();

    }

    private static void startBankMock() {

        WireMockServer bankMock = new WireMockServer(8082);
        bankMock.stubFor(post("/authorizeAndCapture")
                .withRequestBody(containing("555"))
                .willReturn(aResponse()
                        .withHeader("Content-type", "application/json")
                        .withBody("{\"creditCardToken\": \"" + UUID.randomUUID() +"\"}")
                        .withStatus(201)));

        bankMock.stubFor(post("/authorizeAndCapture")
                .withRequestBody(containing("222"))
                .willReturn(aResponse().withStatus(400)));

        bankMock.stubFor(post("/refund")
                .willReturn(aResponse().withStatus(200)));

        bankMock.stubFor(post("/refund")
                .withRequestBody(containing("66666.66"))
                .willReturn(aResponse().withStatus(400)));

        bankMock.start();
    }

}
