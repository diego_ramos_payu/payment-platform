package com.diegoramos.paymentplatform.service.impl;

import com.diegoramos.paymentplatform.mapper.OrderMapper;
import com.diegoramos.paymentplatform.model.ApprovalStatus;
import com.diegoramos.paymentplatform.model.TransactionType;
import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.model.entity.OrderEntity;
import com.diegoramos.paymentplatform.repository.OrderRepository;
import com.diegoramos.paymentplatform.service.api.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public OrderDto persistOrder(OrderDto orderDto) {

        return orderMapper.getDto(orderRepository.save(orderMapper.getEntity(orderDto)));
    }

    @Override
    public OrderDto updateOrder(OrderDto orderDto) {

        return orderMapper.getDto(orderRepository.save(orderMapper.getEntity(orderDto)));
    }

    @Override
    public OrderDto getOrder(String orderId) {

        Optional<OrderEntity> orderEntity = orderRepository.findById(orderId);

        if(orderEntity.isPresent()) {

            return orderMapper.getDto(orderEntity.get());
        } else {

            throw new EntityNotFoundException(String.format("Order with id %s not found", orderId));
        }
    }

    @Override
    public TransactionDto getApprovedAuthorizationAndCaptureTransaction(OrderDto orderDto) {

        TransactionDto transaction = null;

        for (int i = 0; i < orderDto.getTransactions().size() && transaction == null; i++) {

            if(ApprovalStatus.APPROVED.equals(orderDto.getTransactions().get(i).getApprovalStatus()) &&
                    TransactionType.AUTHORIZATION_AND_CAPTURE.equals(orderDto.getTransactions().get(i).getType())) {
                transaction = orderDto.getTransactions().get(i);
            }
        }

        if(transaction != null) {
            return transaction;
        } else {
            throw new EntityNotFoundException(String.format("Order with id %s does not have an approved authorization and capture transaction", orderDto.getOrderId()));
        }
    }

    @Override
    public Double getTotalRefundValue(OrderDto orderDto) {

        Double totalRefundValue = 0.0;

        for (TransactionDto transaction: orderDto.getTransactions()) {

            if(TransactionType.REFUND.equals(transaction.getType()) && ApprovalStatus.APPROVED.equals(transaction.getApprovalStatus())) {
                totalRefundValue += transaction.getNetworkValue();
            }
        }

        return totalRefundValue;
    }
}
