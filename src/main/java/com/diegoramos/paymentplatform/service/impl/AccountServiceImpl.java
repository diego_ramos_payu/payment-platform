package com.diegoramos.paymentplatform.service.impl;

import com.diegoramos.paymentplatform.mapper.AccountMapper;
import com.diegoramos.paymentplatform.model.dto.AccountDto;
import com.diegoramos.paymentplatform.model.entity.AccountEntity;
import com.diegoramos.paymentplatform.repository.AccountRepository;
import com.diegoramos.paymentplatform.service.api.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public AccountDto updateAccount(AccountDto accountDto) {

        return accountMapper.getDto(accountRepository.save(accountMapper.getEntity(accountDto)));
    }

    @Override
    public AccountDto getAccount(String accountId) {

        Optional<AccountEntity> accountEntity = accountRepository.findById(accountId);

        if(accountEntity.isPresent()) {

            return accountMapper.getDto(accountEntity.get());
        } else {

            throw new EntityNotFoundException(String.format("Account with id %s not found", accountId));
        }
    }
}
