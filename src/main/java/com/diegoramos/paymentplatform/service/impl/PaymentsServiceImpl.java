package com.diegoramos.paymentplatform.service.impl;

import com.diegoramos.paymentplatform.model.AntifraudStatus;
import com.diegoramos.paymentplatform.model.ApprovalStatus;
import com.diegoramos.paymentplatform.model.ProcessingStatus;
import com.diegoramos.paymentplatform.model.TransactionType;
import com.diegoramos.paymentplatform.model.dto.*;
import com.diegoramos.paymentplatform.model.entity.BankStatus;
import com.diegoramos.paymentplatform.request.PaymentRequest;
import com.diegoramos.paymentplatform.request.RefundRequest;
import com.diegoramos.paymentplatform.service.api.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class PaymentsServiceImpl implements PaymentsService {

    @Autowired
    private BankService bankService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AntifraudService antifraudService;

    @Override
    public TransactionDto submitCreditCardPayment(final PaymentRequest paymentRequest) {

        AccountDto account = accountService.getAccount(paymentRequest.getAccountId());

        final PayerDto payer = PayerDto.builder()
                .payerId(UUID.randomUUID().toString())
                .creationDate(LocalDateTime.now())
                .name(paymentRequest.getPayer().getName())
                .phoneNumber(paymentRequest.getPayer().getPhoneNumber())
                .email(paymentRequest.getPayer().getEmail())
                .documentType(paymentRequest.getPayer().getDocumentType())
                .documentNumber(paymentRequest.getPayer().getDocumentNumber())
                .country(paymentRequest.getPayer().getCountry())
                .address(paymentRequest.getPayer().getAddress())
                .build();

        final CreditCardDto creditCard = CreditCardDto.builder()
                .creditCardId(UUID.randomUUID().toString())
                .creationDate(LocalDateTime.now())
                .number(paymentRequest.getCreditCard().getNumber())
                .cardHolderName(paymentRequest.getCreditCard().getCardHolderName())
                .expirationDate(paymentRequest.getCreditCard().getExpirationDate())
                .franchise(paymentRequest.getCreditCard().getFranchise())
                .cvv2(paymentRequest.getCreditCard().getCvv2())
                .build();

        final String orderId = UUID.randomUUID().toString();
        final String transactionId = UUID.randomUUID().toString();

        TransactionDto currentTransaction = TransactionDto.builder()
                .transactionId(transactionId)
                .orderId(orderId)
                .creationDate(LocalDateTime.now())
                .type(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .value(paymentRequest.getPurchaseValue())
                .taxValue(paymentRequest.getTaxValue())
                .networkValue(paymentRequest.getPurchaseValue() + paymentRequest.getTaxValue())
                .currency(paymentRequest.getCurrency())
                .creditCard(creditCard)
                .build();

        OrderDto order = OrderDto.builder()
                .orderId(orderId)
                .creationDate(LocalDateTime.now())
                .payer(payer)
                .transactions(List.of(currentTransaction))
                .status(ProcessingStatus.IN_PROGRESS)
                .build();

        transactionService.persistTransaction(currentTransaction);
        orderService.persistOrder(order);

        List<OrderDto> orders = account.getOrders();
        orders.add(order);
        account.setOrders(orders);
        accountService.updateAccount(account);

        currentTransaction = antifraudService.evaluateTransaction(currentTransaction);

        if (AntifraudStatus.ANTIFRAUD_ACCEPTED.equals(currentTransaction.getAntifraudStatus())) {
            currentTransaction = bankService.submitPaymentToBank(currentTransaction, payer);
            if (BankStatus.BANK_ACCEPTED.equals(currentTransaction.getBankStatus())) {
                currentTransaction.setApprovalStatus(ApprovalStatus.APPROVED);
            } else {
                currentTransaction.setApprovalStatus(ApprovalStatus.DECLINED);
                currentTransaction.setErrorMessage("Transaction declined by the bank");
            }
        } else {
            currentTransaction.setApprovalStatus(ApprovalStatus.DECLINED);
            currentTransaction.setErrorMessage("Antifraud validation failed for transaction");
        }

        order.setStatus(ProcessingStatus.COMPLETED);
        orderService.updateOrder(order);

        currentTransaction.setProcessingStatus(ProcessingStatus.COMPLETED);
        transactionService.updateTransaction(currentTransaction);

        return currentTransaction;
    }

    @Override
    public TransactionDto submitRefund(final RefundRequest refundRequest) throws EntityNotFoundException {

        OrderDto storedOrder = orderService.getOrder(refundRequest.getOrderId());
        final TransactionDto authorizationAndCaptureTransaction = orderService.getApprovedAuthorizationAndCaptureTransaction(storedOrder);
        final Double totalRefundValue = orderService.getTotalRefundValue(storedOrder);

        TransactionDto refundTransaction = TransactionDto.builder()
                .transactionId(UUID.randomUUID().toString())
                .orderId(storedOrder.getOrderId())
                .creationDate(LocalDateTime.now())
                .type(TransactionType.REFUND)
                .processingStatus(ProcessingStatus.IN_PROGRESS)
                .value(refundRequest.getRefundValue())
                .networkValue(refundRequest.getRefundValue())
                .currency(refundRequest.getCurrency())
                .build();

        storedOrder.setStatus(ProcessingStatus.IN_PROGRESS);

        List<TransactionDto> newTransactions = storedOrder.getTransactions();
        newTransactions.add(refundTransaction);
        storedOrder.setTransactions(newTransactions);

        orderService.updateOrder(storedOrder);

        if (validateRefund(totalRefundValue, authorizationAndCaptureTransaction, refundRequest)) {

            refundTransaction = bankService.submitRefundToBank(refundTransaction);

            if (BankStatus.BANK_ACCEPTED.equals(refundTransaction.getBankStatus())) {
                refundTransaction.setApprovalStatus(ApprovalStatus.APPROVED);
            } else {
                refundTransaction.setApprovalStatus(ApprovalStatus.DECLINED);
            }
        } else {

            refundTransaction.setApprovalStatus(ApprovalStatus.DECLINED);
            refundTransaction.setErrorMessage(
                    String.format("The value of all the approved refunds for this order (%.2f %s) " +
                                    "plus the value of the requested refund (%.2f %s) is greater than the original purchase value of %.2f %s",
                            totalRefundValue,
                            authorizationAndCaptureTransaction.getCurrency(),
                            refundRequest.getRefundValue(),
                            refundRequest.getCurrency(),
                            authorizationAndCaptureTransaction.getNetworkValue(),
                            authorizationAndCaptureTransaction.getCurrency()));
        }

        storedOrder.setStatus(ProcessingStatus.COMPLETED);
        orderService.updateOrder(storedOrder);

        refundTransaction.setProcessingStatus(ProcessingStatus.COMPLETED);
        refundTransaction = transactionService.updateTransaction(refundTransaction);
        return refundTransaction;
    }

    @Override
    public OrderDto queryPayment(final String orderId) {

        return orderService.getOrder(orderId);
    }

    private boolean validateRefund(final Double totalRefundValue, final TransactionDto authorizationAndCaptureTransaction,
                                   final RefundRequest refundRequest) {

        return authorizationAndCaptureTransaction.getCurrency().equals(refundRequest.getCurrency()) &&
                refundRequest.getRefundValue() <= authorizationAndCaptureTransaction.getNetworkValue() &&
                totalRefundValue + refundRequest.getRefundValue() <= authorizationAndCaptureTransaction.getNetworkValue();
    }
}
