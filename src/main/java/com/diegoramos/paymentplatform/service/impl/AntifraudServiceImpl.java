package com.diegoramos.paymentplatform.service.impl;

import com.diegoramos.paymentplatform.client.antifraud.AntifraudClient;
import com.diegoramos.paymentplatform.client.antifraud.AntifraudResult;
import com.diegoramos.paymentplatform.model.AntifraudStatus;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.service.api.AntifraudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class AntifraudServiceImpl implements AntifraudService {

    @Autowired
    private AntifraudClient antifraudClient;

    @Override
    public TransactionDto evaluateTransaction(final TransactionDto transactionDto) {

        final AntifraudResult antifraudResponse = antifraudClient.validateTransaction(transactionDto);

        return TransactionDto.builder()
                .creationDate(transactionDto.getCreationDate())
                .transactionId(transactionDto.getTransactionId())
                .orderId(transactionDto.getOrderId())
                .type(transactionDto.getType())
                .currency(transactionDto.getCurrency())
                .processingStatus(transactionDto.getProcessingStatus())
                .value(transactionDto.getValue())
                .taxValue(transactionDto.getTaxValue())
                .networkValue(transactionDto.getNetworkValue())
                .creditCard(transactionDto.getCreditCard())
                .antifraudStatus(HttpStatus.OK.equals(antifraudResponse.getStatusCode()) ?
                        AntifraudStatus.ANTIFRAUD_ACCEPTED : AntifraudStatus.ANTIFRAUD_DECLINED)
                .build();
    }
}
