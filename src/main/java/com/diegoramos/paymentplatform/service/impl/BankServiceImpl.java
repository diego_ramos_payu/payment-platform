package com.diegoramos.paymentplatform.service.impl;

import com.diegoramos.paymentplatform.client.bank.BankClient;
import com.diegoramos.paymentplatform.client.bank.BankPaymentResult;
import com.diegoramos.paymentplatform.client.bank.BankRefundResult;
import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.service.api.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankServiceImpl implements BankService {

    @Autowired
    private BankClient bankClient;

    @Override
    public TransactionDto submitPaymentToBank(TransactionDto transaction, PayerDto payer) {

        BankPaymentResult result = bankClient.submitPaymentToBank(transaction, payer);

        return buildTransaction(transaction)
                .bankStatus(result.getBankStatus())
                .creditCardToken(result.getCreditCardToken())
                .build();
    }

    @Override
    public TransactionDto submitRefundToBank(TransactionDto transaction) {

        BankRefundResult result = bankClient.submitRefundToBank(transaction);

        return buildTransaction(transaction)
                .bankStatus(result.getBankStatus())
                .build();
    }

    private TransactionDto.TransactionDtoBuilder buildTransaction(final TransactionDto transaction) {

        return TransactionDto.builder()
                .creationDate(transaction.getCreationDate())
                .transactionId(transaction.getTransactionId())
                .orderId(transaction.getOrderId())
                .type(transaction.getType())
                .currency(transaction.getCurrency())
                .processingStatus(transaction.getProcessingStatus())
                .value(transaction.getValue())
                .taxValue(transaction.getTaxValue())
                .networkValue(transaction.getNetworkValue())
                .creditCard(transaction.getCreditCard())
                .antifraudStatus(transaction.getAntifraudStatus());
    }
}
