package com.diegoramos.paymentplatform.service.impl;

import com.diegoramos.paymentplatform.mapper.TransactionMapper;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.model.entity.TransactionEntity;
import com.diegoramos.paymentplatform.repository.TransactionRepository;
import com.diegoramos.paymentplatform.service.api.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionMapper transactionMapper;

    @Override
    public TransactionDto persistTransaction(TransactionDto transactionDto) {

        return transactionMapper.getDto(transactionRepository.save(transactionMapper.getEntity(transactionDto)));
    }

    @Override
    public TransactionDto updateTransaction(TransactionDto transactionDto) {

        return transactionMapper.getDto(transactionRepository.save(transactionMapper.getEntity(transactionDto)));
    }

    @Override
    public TransactionDto getTransaction(String transactionId) {

        Optional<TransactionEntity> transactionEntity = transactionRepository.findById(transactionId);

        if(transactionEntity.isPresent()) {

            return transactionMapper.getDto(transactionEntity.get());
        } else {

            throw new EntityNotFoundException(String.format("Transaction with id %s not found", transactionId));
        }
    }
}
