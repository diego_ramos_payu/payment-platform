package com.diegoramos.paymentplatform.service.api;

import com.diegoramos.paymentplatform.model.dto.AccountDto;
import org.springframework.stereotype.Service;

@Service
public interface AccountService {

    AccountDto getAccount(final String accountId);

    AccountDto updateAccount(final AccountDto accountDto);
}
