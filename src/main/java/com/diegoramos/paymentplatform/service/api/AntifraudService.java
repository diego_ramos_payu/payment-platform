package com.diegoramos.paymentplatform.service.api;

import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import org.springframework.stereotype.Service;

@Service
public interface AntifraudService {

    TransactionDto evaluateTransaction(final TransactionDto transactionDto);
}
