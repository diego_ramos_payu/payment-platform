package com.diegoramos.paymentplatform.service.api;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import org.springframework.stereotype.Service;

@Service
public interface OrderService {

    OrderDto persistOrder(final OrderDto orderDto);

    OrderDto updateOrder(final OrderDto orderDto);

    OrderDto getOrder(final String orderId);

    TransactionDto getApprovedAuthorizationAndCaptureTransaction(final OrderDto orderDto);

    Double getTotalRefundValue(final OrderDto orderDto);

}
