package com.diegoramos.paymentplatform.service.api;

import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import org.springframework.stereotype.Service;

@Service
public interface BankService {


    TransactionDto submitPaymentToBank(TransactionDto transaction, PayerDto payer);

    TransactionDto submitRefundToBank(TransactionDto transaction);

}
