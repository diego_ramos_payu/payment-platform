package com.diegoramos.paymentplatform.service.api;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.request.PaymentRequest;
import com.diegoramos.paymentplatform.request.RefundRequest;
import org.springframework.stereotype.Service;

@Service
public interface PaymentsService {

    TransactionDto submitCreditCardPayment(final PaymentRequest paymentRequest);

    TransactionDto submitRefund(final RefundRequest refundRequest);

    OrderDto queryPayment(final String orderId);
}
