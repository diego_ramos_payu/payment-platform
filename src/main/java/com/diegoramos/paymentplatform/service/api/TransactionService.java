package com.diegoramos.paymentplatform.service.api;

import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import org.springframework.stereotype.Service;

@Service
public interface TransactionService {

    TransactionDto persistTransaction(final TransactionDto transactionDto);

    TransactionDto updateTransaction(final TransactionDto transactionDto);

    TransactionDto getTransaction(final String transactionId);
}
