package com.diegoramos.paymentplatform.client.bank.mock;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * The mock bank payment response
 */
@NoArgsConstructor
@Getter
@Setter
public class MockBankPaymentResponse implements Serializable {

    private String creditCardToken;

}
