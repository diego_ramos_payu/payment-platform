package com.diegoramos.paymentplatform.client.bank.mock;

import com.diegoramos.paymentplatform.client.bank.*;
import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.model.entity.BankStatus;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * The bank client for the mock service
 */
@Service
public class BankClientMock implements BankClient {

    private static final String URL = "http://localhost:8082";

    @Override
    public BankPaymentResult submitPaymentToBank(TransactionDto transaction, PayerDto payer) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<MockBankPaymentRequest> request = new HttpEntity<>(MockBankPaymentRequest.builder()
                .creditCardExpirationDate(transaction.getCreditCard().getExpirationDate())
                .creditCardCvv2(transaction.getCreditCard().getCvv2())
                .creditCardCardholderName(transaction.getCreditCard().getCardHolderName())
                .creditCardNumber(transaction.getCreditCard().getNumber())
                .purchaseValue(transaction.getNetworkValue())
                .currency(transaction.getCurrency().name())
                .payerAddress(payer.getAddress())
                .payerDocumentNumber(payer.getDocumentNumber())
                .payerDocumentType(payer.getDocumentType().name())
                .payerName(payer.getName())
                .build());
        try {
            ResponseEntity<MockBankPaymentResponse> response = restTemplate.postForEntity(URL + "/authorizeAndCapture", request, MockBankPaymentResponse.class);
            return new BankPaymentResult(response.getBody().getCreditCardToken(),
                    HttpStatus.CREATED.equals(response.getStatusCode()) ? BankStatus.BANK_ACCEPTED : BankStatus.BANK_DECLINED);
        } catch (HttpClientErrorException e) {
            return new BankPaymentResult("", BankStatus.BANK_DECLINED);
        }
    }

    @Override
    public BankRefundResult submitRefundToBank(TransactionDto transaction) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<MockBankRefundRequest> request = new HttpEntity<>(MockBankRefundRequest.builder()
                .creditCardToken(transaction.getCreditCardToken())
                .refundValue(transaction.getNetworkValue())
                .currency(transaction.getCurrency().name())
                .build());
        try {
            ResponseEntity<MockBankRefundResponse> response = restTemplate.postForEntity(URL + "/refund", request, MockBankRefundResponse.class);
            return new BankRefundResult(HttpStatus.OK.equals(response.getStatusCode()) ? BankStatus.BANK_ACCEPTED : BankStatus.BANK_DECLINED);
        } catch (HttpClientErrorException e) {
            return new BankRefundResult(BankStatus.BANK_DECLINED);
        }
    }
}
