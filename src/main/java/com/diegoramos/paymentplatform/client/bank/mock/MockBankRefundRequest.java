package com.diegoramos.paymentplatform.client.bank.mock;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * The mock bank refund request
 */
@Builder
@Getter
public class MockBankRefundRequest implements Serializable {

    private final String creditCardToken;

    private final Double refundValue;

    private final String currency;
}
