package com.diegoramos.paymentplatform.client.bank.mock;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * The mock bank payment request
 */
@Getter
@Builder
public class MockBankPaymentRequest implements Serializable {

    private final String creditCardNumber;

    private final String creditCardCvv2;

    private final String creditCardExpirationDate;

    private final String creditCardCardholderName;

    private final String payerName;

    private final String payerAddress;

    private final String payerDocumentNumber;

    private final String payerDocumentType;

    private final Double purchaseValue;

    private final String currency;

}
