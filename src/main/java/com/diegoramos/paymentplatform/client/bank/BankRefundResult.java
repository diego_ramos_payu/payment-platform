package com.diegoramos.paymentplatform.client.bank;

import com.diegoramos.paymentplatform.model.entity.BankStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The bank refund result
 */
@Getter
@AllArgsConstructor
public class BankRefundResult {

    /**
     * The bank refund status
     */
    private final BankStatus bankStatus;
}
