package com.diegoramos.paymentplatform.client.bank;

import com.diegoramos.paymentplatform.model.dto.PayerDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import org.springframework.stereotype.Service;

/**
 * The bank client interface
 */
@Service
public interface BankClient {

    /**
     * Submits a payment to a bank service
     *
     * @param transaction the transaction to submit
     * @param payer the payer info to submit
     * @return a result with the bank response
     */
    BankPaymentResult submitPaymentToBank(TransactionDto transaction, PayerDto payer);

    /**
     * Submits a refund to a bank service
     *
     * @param transaction the refund transaction
     * @return a result with the bank response
     */
    BankRefundResult submitRefundToBank(TransactionDto transaction);

}
