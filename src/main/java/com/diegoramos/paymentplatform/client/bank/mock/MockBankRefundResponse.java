package com.diegoramos.paymentplatform.client.bank.mock;

import java.io.Serializable;

/**
 * The mock bank refund response
 */
public class MockBankRefundResponse implements Serializable {

    // The mock response doesn't have a body, but the class is necessary for the RestTemplate client implementation

}
