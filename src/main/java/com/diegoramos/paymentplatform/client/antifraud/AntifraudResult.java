package com.diegoramos.paymentplatform.client.antifraud;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * The antifraud result. For this demo, the status code is enough to determine the antifraud result
 */
@Getter
@AllArgsConstructor
public class AntifraudResult {

    private HttpStatus statusCode;
}
