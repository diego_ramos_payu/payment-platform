package com.diegoramos.paymentplatform.client.antifraud.mock;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * Antifraud mock request
 */
@Builder
@Getter
public class MockAntifraudRequest implements Serializable {

    private final String creditCardNumber;

    private final String creditCardCvv2;

    private final String creditCardExpirationDate;

    private final String creditCardCardholderName;

    private final Double purchaseValue;

    private final String currency;

}
