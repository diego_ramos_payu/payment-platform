package com.diegoramos.paymentplatform.client.antifraud.mock;

/**
 * Antifraud mock response
 */
public class MockAntifraudResponse {

 // The mock response doesn't have a body, but the class is necessary for the RestTemplate client implementation

}
