package com.diegoramos.paymentplatform.client.antifraud;

import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import org.springframework.stereotype.Service;

/**
 * Antifraud client interface
 */
@Service
public interface AntifraudClient {

    /**
     * Validates a transaction with an antifraud HTTP service
     *
     * @param transaction the transaction to validate
     * @return an AntifraudResult indicating the andtifraud validation result
     */
    AntifraudResult validateTransaction(TransactionDto transaction);

}
