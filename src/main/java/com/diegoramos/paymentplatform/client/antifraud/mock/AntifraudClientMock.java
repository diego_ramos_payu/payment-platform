package com.diegoramos.paymentplatform.client.antifraud.mock;

import com.diegoramos.paymentplatform.client.antifraud.AntifraudClient;
import com.diegoramos.paymentplatform.client.antifraud.AntifraudResult;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Antifraud client for the mock service
 */
@Service
public class AntifraudClientMock implements AntifraudClient {

    private static final String url = "http://localhost:8081";

    @Override
    public AntifraudResult validateTransaction(TransactionDto transaction) {

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<MockAntifraudRequest> request = new HttpEntity<>(MockAntifraudRequest.builder()
                .creditCardExpirationDate(transaction.getCreditCard().getExpirationDate())
                .creditCardCvv2(transaction.getCreditCard().getCvv2())
                .creditCardCardholderName(transaction.getCreditCard().getCardHolderName())
                .creditCardNumber(transaction.getCreditCard().getNumber())
                .purchaseValue(transaction.getNetworkValue())
                .currency(transaction.getCurrency().name())
                .build());
        try {
            ResponseEntity<MockAntifraudResponse> response = restTemplate.postForEntity(url + "/validate", request, MockAntifraudResponse.class);
            return new AntifraudResult(response.getStatusCode());
        } catch (HttpClientErrorException e) {
            return new AntifraudResult(HttpStatus.BAD_REQUEST);
        }
    }
}
