package com.diegoramos.paymentplatform.request;

import com.diegoramos.paymentplatform.model.Currency;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Builder
public class RefundRequest {

    @NotEmpty
    private String orderId;

    @Min(1)
    private Double refundValue;

    private Currency currency;
}
