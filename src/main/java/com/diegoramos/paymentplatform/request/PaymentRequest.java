package com.diegoramos.paymentplatform.request;

import com.diegoramos.paymentplatform.model.Currency;
import com.diegoramos.paymentplatform.model.dto.CreditCardDto;
import com.diegoramos.paymentplatform.model.dto.PayerDto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Builder
public class PaymentRequest {

    @Min(1)
    private Double purchaseValue;

    @Min(0)
    private Double taxValue;

    private Currency currency;

    @NotEmpty
    private String accountId;

    @Valid
    private CreditCardDto creditCard;

    @Valid
    private PayerDto payer;

}
