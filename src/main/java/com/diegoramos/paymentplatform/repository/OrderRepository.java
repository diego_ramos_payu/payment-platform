package com.diegoramos.paymentplatform.repository;

import com.diegoramos.paymentplatform.model.entity.OrderEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<OrderEntity, String> {
}
