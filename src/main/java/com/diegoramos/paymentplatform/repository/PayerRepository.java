package com.diegoramos.paymentplatform.repository;

import com.diegoramos.paymentplatform.model.entity.PayerEntity;
import org.springframework.data.repository.CrudRepository;

public interface PayerRepository extends CrudRepository<PayerEntity, String> {
}
