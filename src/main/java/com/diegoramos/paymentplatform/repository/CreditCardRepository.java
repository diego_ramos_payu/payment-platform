package com.diegoramos.paymentplatform.repository;

import com.diegoramos.paymentplatform.model.entity.CreditCardEntity;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCardEntity, String> {
}
