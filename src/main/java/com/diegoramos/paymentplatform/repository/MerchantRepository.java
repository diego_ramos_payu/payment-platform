package com.diegoramos.paymentplatform.repository;

import com.diegoramos.paymentplatform.model.entity.MerchantEntity;
import org.springframework.data.repository.CrudRepository;

public interface MerchantRepository extends CrudRepository<MerchantEntity, String> {
}
