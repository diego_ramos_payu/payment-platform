package com.diegoramos.paymentplatform.repository;

import com.diegoramos.paymentplatform.model.entity.TransactionEntity;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<TransactionEntity, String> {
}
