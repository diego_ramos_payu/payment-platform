package com.diegoramos.paymentplatform.repository;

import com.diegoramos.paymentplatform.model.entity.AccountEntity;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<AccountEntity, String> {
}
