package com.diegoramos.paymentplatform.controller;

import com.diegoramos.paymentplatform.model.dto.OrderDto;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.request.PaymentRequest;
import com.diegoramos.paymentplatform.request.RefundRequest;
import com.diegoramos.paymentplatform.response.PaymentResponse;
import com.diegoramos.paymentplatform.response.QueryPaymentResponse;
import com.diegoramos.paymentplatform.response.RefundResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * The payments controller interface
 */
public interface PaymentsController {

    /**
     * Submits a credit card payment to the platform
     *
     * @param paymentRequest the payment request
     * @return a response with a 201 HTTP code if the submission is successful,
     * or a response with a 400 HTTP code otherwise
     */
    ResponseEntity<PaymentResponse> submitCreditCardPayment(@RequestBody final PaymentRequest paymentRequest);

    /**
     * Refunds a payment
     *
     * @param refundRequest the refund request
     * @return a response with a 201 HTTP code if the refund is successful,
     * a 400 HTTP code if it's not successful,
     * or a 404 HTTP code if there's not an order to refund with the provided orderId
     */
    ResponseEntity<RefundResponse> refundPayment(@RequestBody final RefundRequest refundRequest);

    /**
     * Returns the information of an order
     *
     * @param orderId the orderId
     * @return a 200 HTTP response and the order information,
     * or a 404 response if the order is not found
     */
    ResponseEntity<QueryPaymentResponse> queryPayment(@PathVariable final String orderId);

}
