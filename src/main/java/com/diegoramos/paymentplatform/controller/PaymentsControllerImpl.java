package com.diegoramos.paymentplatform.controller;

import com.diegoramos.paymentplatform.model.ApprovalStatus;
import com.diegoramos.paymentplatform.model.dto.TransactionDto;
import com.diegoramos.paymentplatform.request.PaymentRequest;
import com.diegoramos.paymentplatform.request.RefundRequest;
import com.diegoramos.paymentplatform.response.PaymentResponse;
import com.diegoramos.paymentplatform.response.QueryPaymentResponse;
import com.diegoramos.paymentplatform.response.RefundResponse;
import com.diegoramos.paymentplatform.service.api.PaymentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/payments")
public class PaymentsControllerImpl implements PaymentsController {

    @Autowired
    private PaymentsService paymentsService;

    /**
     * Submits a credit card payment to the platform
     *
     * @param paymentRequest the payment request
     * @return a response with a 201 HTTP code if the submission is successful,
     * or a response with a 400 HTTP code otherwise
     */
    @PostMapping
    @Override
    public ResponseEntity<PaymentResponse> submitCreditCardPayment(@Valid @RequestBody PaymentRequest paymentRequest) {
        try {
            final TransactionDto transaction = paymentsService.submitCreditCardPayment(paymentRequest);

            if (ApprovalStatus.APPROVED.equals(transaction.getApprovalStatus())) {
                return ResponseEntity.status(HttpStatus.CREATED).body(new PaymentResponse(transaction.getOrderId(),
                        transaction.getTransactionId(),
                        transaction.getApprovalStatus(), ""));
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new PaymentResponse(transaction.getOrderId(),
                        transaction.getTransactionId(),
                        transaction.getApprovalStatus(),
                        transaction.getErrorMessage()));
            }
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new PaymentResponse(null,
                    null,
                    null,
                    e.getMessage()));
        }

    }

    /**
     * Refunds a payment
     *
     * @param refundRequest the refund request
     * @return a response with a 201 HTTP code if the refund is successful,
     * a 400 HTTP code if it's not successful,
     * or a 404 HTTP code if there's not an order to refund with the provided orderId
     */
    @PutMapping
    @Override
    public ResponseEntity<RefundResponse> refundPayment(@Valid @RequestBody RefundRequest refundRequest) {


        try {
            final TransactionDto transaction = paymentsService.submitRefund(refundRequest);

            if (ApprovalStatus.APPROVED.equals(transaction.getApprovalStatus())) {
                return ResponseEntity.status(HttpStatus.CREATED).body(new RefundResponse(transaction, ""));
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new RefundResponse(transaction, ""));
            }
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new RefundResponse(null, e.getMessage()));
        }
    }

    /**
     * Returns the information of an order
     *
     * @param orderId the orderId
     * @return a 200 HTTP response and the order information,
     * or a 404 response if the order is not found
     */
    @GetMapping(path = "/{orderId}")
    @Override
    public ResponseEntity<QueryPaymentResponse> queryPayment(@PathVariable("orderId") String orderId) {

        try {
            return ResponseEntity.ok(new QueryPaymentResponse(paymentsService.queryPayment(orderId), ""));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new QueryPaymentResponse(null, e.getMessage()));
        }
    }
}
